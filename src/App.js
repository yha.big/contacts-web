import React, { Fragment, useState, useEffect } from "react";

import Formulario from "./components/Formulario";
import Contact from "./components/Contact";

function App() {
  let savedContacts = JSON.parse(localStorage.getItem("contacts"));

  if (!savedContacts) {
    savedContacts = [];
  }

  const [contacts, setContacts] = useState(savedContacts);

  useEffect(() => {
    let savedContacts = JSON.parse(localStorage.getItem("contacts"));

    if (savedContacts) {
      localStorage.setItem("contacts", JSON.stringify(contacts));
    } else {
      localStorage.setItem("contacts", JSON.stringify([]));
    }
  }, [contacts]);

  const createContact = (contact) => {
    setContacts([...contacts, contact]);
  };

  const deleteContact = (id) => {
    const newContact = contacts.filter((contact) => contact.id !== id);
    setContacts(newContact);
  };

  const title =
    contacts.length === 0 ? "No hay contactos" : "Administra tus contactos";

  return (
    <Fragment>
      <h1>Contactos</h1>
      <div className="container">
        <div className="one-half column">
          <Formulario createContact={createContact} />
        </div>
        <div className="one-half column">
          <h2>{title}</h2>
          {contacts.map((contact) => (
            <Contact
              key={contact.id}
              contact={contact}
              deleteContact={deleteContact}
            />
          ))}
        </div>
      </div>
    </Fragment>
  );
}

export default App;
