import React, { Fragment, useState } from "react";
import { v4 as uuidv4 } from "uuid";
import PropTypes from "prop-types";

const Formulario = ({ createContact }) => {
  const [contact, setContact] = useState(emptyContact);

  const handleChange = (e) => {
    setContact({
      ...contact,
      [e.target.name]: e.target.value,
    });
  };

  const [error, setError] = useState(false);

  const { name, telephone, email, address } = contact;

  const submitContact = (e) => {
    e.preventDefault();

    if (
      name.trim() === "" ||
      telephone.trim() === "" ||
      email.trim() === "" ||
      address.trim() === ""
    ) {
      setError(true);
      return;
    }

    setError(false);

    contact.id = uuidv4();

    createContact(contact);

    setContact(emptyContact);
  };

  return (
    <Fragment>
      <h2>Crear contacto</h2>

      {error ? (
        <p className="alerta-error">Todos los campos son obligatorios</p>
      ) : null}

      <form onSubmit={submitContact}>
        <label>Nombre</label>
        <input
          type="text"
          name="name"
          className="u-full-width"
          placeholder="Nombre"
          onChange={handleChange}
          value={name}
        />

        <label>Teléfono</label>
        <input
          type="text"
          name="telephone"
          className="u-full-width"
          placeholder="Teléfono"
          onChange={handleChange}
          value={telephone}
        />

        <label>Email</label>
        <input
          type="text"
          name="email"
          className="u-full-width"
          placeholder="Email"
          onChange={handleChange}
          value={email}
        />

        <label>Dirección</label>
        <input
          type="text"
          name="address"
          className="u-full-width"
          placeholder="Dirección"
          onChange={handleChange}
          value={address}
        />

        <button type="submit" className="u-full-width button-primary">
          Aceptar
        </button>
      </form>
    </Fragment>
  );
};

// Los valores de cada propiedad deben ser iguales al value de cada input
const emptyContact = {
  name: "",
  telephone: "",
  email: "",
  address: "",
};

// Documenta el componente
Formulario.propTypes = {
  createContact: PropTypes.func.isRequired,
};

export default Formulario;
