import React from "react";
import PropTypes from "prop-types";

const Contact = ({ contact, deleteContact }) => {
  return (
    <div className="contact">
      <p>
        Nombre: <span>{contact.name}</span>
      </p>
      <p>
        Teléfono: <span>{contact.telephone}</span>
      </p>
      <p>
        Email: <span>{contact.email}</span>
      </p>
      <p>
        Dirección: <span>{contact.address}</span>
      </p>

      <button
        className="button eliminar u-full-width"
        onClick={() => deleteContact(contact.id)}
      >
        Eliminar &times;
      </button>
    </div>
  );
};

Contact.propTypes = {
  contact: PropTypes.object.isRequired,
  deleteContact: PropTypes.func.isRequired,
};

export default Contact;
